# Lublin model

This project uses the original code for the lublin model available on
the parallel workload archive website.

This version does not modify the model directly, however it mades some
parameters accecible as input parameters.

The number of job to generate.
The activation of the Interactive jobs
The probability for serial job.

In addition to facilitate reproducibility, I added the log of
the random seed into the generated SWF.
